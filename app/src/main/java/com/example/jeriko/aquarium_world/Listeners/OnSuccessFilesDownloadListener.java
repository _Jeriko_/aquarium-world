package com.example.jeriko.aquarium_world.Listeners;

import com.example.jeriko.aquarium_world.Beans.DataType;

import java.util.EventListener;
import java.util.List;

public interface OnSuccessFilesDownloadListener extends EventListener {
    void onSuccess(List<DataType> list);
}
