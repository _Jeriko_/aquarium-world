package com.example.jeriko.aquarium_world.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.Seller;
import com.example.jeriko.aquarium_world.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class SellersFragment extends Fragment {
    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayout);

        checkSellersTable();

        return view;
    }
    private void updateUI(ArrayList<Seller> list){
        recyclerView.setAdapter(new SellerAdapter(list));
    }
    private void checkSellersTable(){
        //DBHelper dbHelper = new DBHelper(getContext());
        //SQLiteDatabase db = dbHelper.getReadableDatabase();

        //Cursor cursor = db.query("sellers",)
        ArrayList<Seller> list = new ArrayList<>();

        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        final int[]index = {0};

        firestore.collection("users").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        for(QueryDocumentSnapshot qs : task.getResult()){
                            if(qs.getData().get("status").toString()=="true")
                                list.add(new Seller(index[0]++,qs.getData().get("name").toString(),null));
                        }
                        updateUI(list);
                    }
                });
    }
    private class SellerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout sellerLayout;
        private TextView sellerName;
        //private TextView sellerSigninDate;
        private ImageView sellerImage;

        public SellerHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.seller,container,false));

            sellerLayout = itemView.findViewById(R.id.seller_layout);
            sellerName = itemView.findViewById(R.id.seller_name);
            //sellerSigninDate = itemView.findViewById(R.id.seller_signin_date);
            sellerImage = itemView.findViewById(R.id.seller_image);

            sellerLayout.setOnClickListener(this);
        }
        private void bind(Seller seller){
            sellerName.setText(seller.getName());
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(),"CLICK",Toast.LENGTH_LONG).show();
        }
    }
    private class SellerAdapter extends RecyclerView.Adapter<SellerHolder>{
        private ArrayList<Seller> list;
        public SellerAdapter(ArrayList<Seller> list){
            this.list = list;
        }
        @NonNull
        @Override
        public SellerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new SellerHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull SellerHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
