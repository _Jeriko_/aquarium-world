package com.example.jeriko.aquarium_world.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Fragments.AboutAppFragment;
import com.example.jeriko.aquarium_world.Fragments.AddProductFragment;
import com.example.jeriko.aquarium_world.Fragments.BasketFragment;
import com.example.jeriko.aquarium_world.Fragments.PersonalOfficeFragment;
import com.example.jeriko.aquarium_world.Fragments.SellersFragment;
import com.example.jeriko.aquarium_world.Fragments.SettingsFragment;
import com.example.jeriko.aquarium_world.Fragments.ShopFragment;
import com.example.jeriko.aquarium_world.Fragments.TypesFragment;
import com.example.jeriko.aquarium_world.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth mAuth;
    private FragmentManager manager = getSupportFragmentManager();
    private FirebaseUser user;
    private boolean rights;
    private LinearLayout personalInformation;
    private LinearLayout changeName;
    private LinearLayout changePhoneNumber;
    private LinearLayout changeEmail;
    private LinearLayout changePassword;
    private String name;
    private String phone;
    private boolean type;
    private String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        rights = getIntent().getBooleanExtra("AsUser",false);

        NavigationView nv = findViewById(R.id.nav_view);
        nv.getMenu().findItem(R.id.nav_encyclopedia).setChecked(true);
        View header = nv.getHeaderView(0);
        if(rights) {
            user = mAuth.getCurrentUser();
            email = getIntent().getStringExtra("E-mail");
            name = getIntent().getStringExtra("Name");

            ((TextView)header.findViewById(R.id.user_email)).setText(email);
            ((TextView)header.findViewById(R.id.user_name)).setText(name);
            nv.getMenu().findItem(R.id.nav_add_product).setVisible(true);
            nv.getMenu().findItem(R.id.nav_basket).setCheckable(true);
            nv.getMenu().findItem(R.id.nav_sellers).setCheckable(true);
            nv.getMenu().findItem(R.id.nav_personal_office).setCheckable(true);

            FirebaseFirestore firestore = FirebaseFirestore.getInstance();
            CollectionReference cf = firestore.collection("users");

            firestore.collection("users").get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            //Toast.makeText(getApplication(),"Successfully",Toast.LENGTH_LONG).show();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    QueryDocumentSnapshot userDoc=null;
                    for(QueryDocumentSnapshot qs : task.getResult()){
                        if(qs.getData().get("userId").toString().compareTo(user.getUid())==0){
                            userDoc = qs;
                            Log.i("USER","Find");
                            break;
                        }
                    }
                    phone = userDoc.getData().get("phone").toString();
                }
            });
        }else{
            ((TextView)header.findViewById(R.id.user_email)).setText("Гость");
            ((TextView)header.findViewById(R.id.user_name)).setText("Гость");
            nv.getMenu().findItem(R.id.nav_personal_office).setCheckable(false);
            nv.getMenu().findItem(R.id.nav_basket).setCheckable(false);
            nv.getMenu().findItem(R.id.nav_sellers).setCheckable(false);
            nv.getMenu().findItem(R.id.nav_logout).setVisible(false);
            nv.getMenu().findItem(R.id.nav_login).setVisible(true);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setTitle(R.string.encyclopedia);
        TypesFragment typesFragment = new TypesFragment();
        manager.beginTransaction()
                .add(R.id.container_recycler,typesFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            getSupportFragmentManager().popBackStackImmediate();
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar species clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        NavigationView nv = findViewById(R.id.nav_view);
        int id = item.getItemId();
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (id == R.id.nav_encyclopedia) {
            toolbar.setTitle("Энциклопедия");
            Snackbar.make(this.getCurrentFocus(),"Encyclopedia",Snackbar.LENGTH_LONG).show();
                startFragment(new TypesFragment());
        } else if (id == R.id.nav_personal_office) {
            if(rights) {
                toolbar.setTitle(R.string.personal_office);
                Bundle bundle = new Bundle();

                bundle.putString("Name",name);
                bundle.putString("Phone",phone);
                bundle.putString("Email",email);

                PersonalOfficeFragment personalOfficeFragment = new PersonalOfficeFragment();
                personalOfficeFragment.setArguments(bundle);
                startFragment(personalOfficeFragment);
            }
            else
                Toast.makeText(getApplication(),"Войдите, чтобы использовать в кабинет!",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_basket) {
            if(rights) {
                toolbar.setTitle(R.string.basket);
                startFragment(new BasketFragment());
            } else
                Toast.makeText(getApplication(),"Войдите, чтобы использовать корзину!",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_shop) {
            toolbar.setTitle(R.string.shop);
            Snackbar.make(this.getCurrentFocus(),"Shop",Snackbar.LENGTH_LONG).show();
                startFragment(new ShopFragment());
        } else if (id == R.id.nav_sellers) {
            if(rights) {
                toolbar.setTitle(R.string.sellers);
                startFragment(new SellersFragment());
            } else
                Toast.makeText(getApplication(),"Войдите, чтобы просмотреть продавцов!",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_settings) {
            toolbar.setTitle(R.string.settings);
            Snackbar.make(this.getCurrentFocus(),"Settings",Snackbar.LENGTH_LONG).show();
                startFragment(new SettingsFragment());
        } else if (id == R.id.nav_about) {
            toolbar.setTitle(R.string.about_app);
            Snackbar.make(this.getCurrentFocus(),"About app",Snackbar.LENGTH_LONG).show();
                startFragment(new AboutAppFragment());
        } else if (id == R.id.nav_logout) {
            if(rights)
                logout();
            else
                Toast.makeText(getApplication(),"Чтобы выйти, нужно зарегистрироваться и войти!",Toast.LENGTH_LONG).show();
        } else if(id == R.id.nav_add_product){
            toolbar.setTitle(R.string.add_new_product);
            Snackbar.make(getCurrentFocus(),"Add new products",Snackbar.LENGTH_LONG).show();
            startFragment(new AddProductFragment());
        } else if(id == R.id.nav_login){
            startActivity(new Intent(this,LoginActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
    private void startFragment(Fragment fragment){
        manager.beginTransaction()
                .replace(R.id.container_recycler, fragment)
                .addToBackStack(null)
                .commit();
    }
    private void logout(){
        if(mAuth.getCurrentUser()!=null)
         mAuth.signOut();

            startActivity(new Intent(this,LoginActivity.class));
    }
    /*public void checkStatus(View view){
        TextView statusView = findViewById(R.id.verification_text);

        FirebaseUser user = mAuth.getCurrentUser();
        user.reload();
        if(user.isEmailVerified())
            statusView.setText(R.string.verification_status_satisfy);
        else
            Toast.makeText(this,"Еще не подтверждено!",Toast.LENGTH_LONG).show();
    }*/
    public void continueAsUser(View view){
        FirebaseUser user = mAuth.getCurrentUser();

        String name = user.getDisplayName();
        String email = user.getEmail();
        boolean asUser = true;

        Intent main = new Intent(this,MainActivity.class);
        main.putExtra("Name",name);
        main.putExtra("E-mail",email);
        main.putExtra("AsUser",asUser);

        startActivity(main);
    }

    public void changeNameLayout(View view){
        if(personalInformation==null)
            personalInformation = findViewById(R.id.personal_information);
        personalInformation.setVisibility(View.GONE);

        if(changeName==null)
            changeName = findViewById(R.id.change_name_layout);
        changeName.setVisibility(View.VISIBLE);
    }
    public void changePhoneLayout(View view){
        if(personalInformation==null)
            personalInformation = findViewById(R.id.personal_information);
        personalInformation.setVisibility(View.GONE);

        if(changePhoneNumber==null)
            changePhoneNumber = findViewById(R.id.change_phone_layout);
        changePhoneNumber.setVisibility(View.VISIBLE);
    }
    public void changeEmailLayout(View view){
        if(personalInformation==null)
            personalInformation = findViewById(R.id.personal_information);
        personalInformation.setVisibility(View.GONE);

        if(changeEmail==null)
            changeEmail = findViewById(R.id.change_email_layout);
        changeEmail.setVisibility(View.VISIBLE);
    }
    public void changePasswordLayout(View view){
        if(personalInformation==null)
            personalInformation = findViewById(R.id.personal_information);
        personalInformation.setVisibility(View.GONE);

        if(changePassword==null)
            changePassword = findViewById(R.id.change_password_layout);
        changePassword.setVisibility(View.VISIBLE);
    }
    public void cancelNameChanging(View view){
        changeName.setVisibility(View.INVISIBLE);
        personalInformation.setVisibility(View.VISIBLE);
    }
    public void cancelPhoneChanging(View view){
        changePhoneNumber.setVisibility(View.INVISIBLE);
        personalInformation.setVisibility(View.VISIBLE);
    }
    public void cancelEmailChanging(View view){
        changeEmail.setVisibility(View.INVISIBLE);
        personalInformation.setVisibility(View.VISIBLE);
    }
    public void cancelPasswordChanging(View view){
        changePassword.setVisibility(View.INVISIBLE);
        personalInformation.setVisibility(View.VISIBLE);
    }
}