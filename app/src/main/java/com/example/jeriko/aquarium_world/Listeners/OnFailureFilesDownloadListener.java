package com.example.jeriko.aquarium_world.Listeners;

import java.util.EventListener;

public interface OnFailureFilesDownloadListener extends EventListener {
    void onFailure(String message);
}
