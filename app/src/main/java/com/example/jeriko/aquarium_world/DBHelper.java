package com.example.jeriko.aquarium_world;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.Beans.PlantAlphabet;

import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "S_info";
    private static final int DB_VERSION = 7;
    private String types = "CREATE TABLE IF NOT EXISTS types (_id INTEGER PRIMARY KEY, title TEXT, image BLOB);";
    private String families = "CREATE TABLE IF NOT EXISTS families (_id INTEGER PRIMARY KEY, title TEXT, image BLOB);";
    private String aquariums = "CREATE TABLE IF NOT EXISTS aquariums (_id INTEGER PRIMARY KEY, title TEXT, image BLOB);";
    private String plants = "CREATE TABLE IF NOT EXISTS plants (_id INTEGER PRIMARY KEY, letter TEXT, count INTEGER, image BLOB);";
    private String accessories = "CREATE TABLE IF NOT EXISTS accessories (_id INTEGER PRIMARY KEY, title TEXT, image BLOB);";
    private String fish = "CREATE TABLE IF NOT EXISTS fish (_id INTEGER PRIMARY KEY, name TEXT, family TEXT, image BLOB,image_name TEXT);";
    public DBHelper(Context context) {
        super(context,DB_NAME,null,DB_VERSION);
    }

    /*public DBHelper(Context context){

    }*/
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(types);
        db.execSQL(families);
        db.execSQL(aquariums);
        db.execSQL(plants);
        db.execSQL(accessories);
        db.execSQL(fish);

        for(PlantAlphabet pa : PlantsAlphabetData.get().getList()){
            ContentValues values = new ContentValues();

            values.put("_id",pa.getId());
            values.put("letter",pa.getLetter());
            values.put("count",pa.getCount());
            values.put("image",pa.getImage());

            db.insert("plants",null,values);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion<newVersion) {
            db.execSQL(aquariums);
            db.execSQL(plants);

            for (PlantAlphabet pa : PlantsAlphabetData.get().getList()) {
                ContentValues values = new ContentValues();

                values.put("_id", pa.getId());
                values.put("letter", pa.getLetter());
                values.put("count", pa.getCount());
                values.put("image", pa.getImage());

                db.insert("plants", null, values);
            }
            db.execSQL(accessories);
            db.execSQL(fish);

            db.execSQL("ALTER TABLE fish ADD COLUMN image_name TEXT");
        }
    }
    public void addData(String db,List<DataType> data){
        SQLiteDatabase database = this.getWritableDatabase();

        switch (db) {
            case "types":
            case "families":
            case "aquariums":
            case "accessories":

                for (DataType dt : data) {
                    ContentValues newValues = new ContentValues();
                    newValues.put("_id", dt.getId());
                    newValues.put("title",dt.getTitle());
                    newValues.put("image",dt.getImage());

                    database.insert(db, null, newValues);
                }
                break;
            case "fish":
                for(DataType dt : data){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("_id",dt.getId());
                    contentValues.put("name",dt.getName());
                    contentValues.put("family",dt.getFamily());
                    contentValues.put("image_name",dt.getImageName());
                    contentValues.put("image",dt.getImage());

                    database.insert(db,null,contentValues);
                }
        }
    }
}
