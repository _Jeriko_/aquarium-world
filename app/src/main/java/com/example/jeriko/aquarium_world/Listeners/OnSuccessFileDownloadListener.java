package com.example.jeriko.aquarium_world.Listeners;

import java.io.File;

public interface OnSuccessFileDownloadListener {
    void OnSuccess(File file);
}
