package com.example.jeriko.aquarium_world.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.R;

public class PersonalOfficeFragment extends Fragment {
    private Bundle bundle;
    private TextView userName;
    private TextView userPhone;
    private TextView userEmail;
    private TextView userPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        bundle = getArguments();

        return inflater.inflate(R.layout.personal_office,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        userName = getView().findViewById(R.id.user_name_po);
        userPhone = getView().findViewById(R.id.user_phone_po);
        userEmail = getView().findViewById(R.id.user_email_po);
        userPassword = getView().findViewById(R.id.user_password_po);

        updateUI();
    }

    private void updateUI(){
        userName.setText(bundle.getString("Name"));
        userPhone.setText(bundle.getString("Phone"));
        userEmail.setText(bundle.getString("Email"));
        userPassword.setText("********");
    }
}
