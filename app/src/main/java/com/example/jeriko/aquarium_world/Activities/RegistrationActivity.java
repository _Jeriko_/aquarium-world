package com.example.jeriko.aquarium_world.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private String name;
    private String phone_number;
    private String email;
    private String password;
    private String password_again;
    private boolean status;
    private Bitmap uAvatar;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();

        ImageView pickAvatar = findViewById(R.id.registration_pick_avatar);
        pickAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent avatarPicker = new Intent(Intent.ACTION_GET_CONTENT);
                avatarPicker.setAction("image/*");
                startActivityForResult(Intent.createChooser(avatarPicker,"Select yout avatar"),1);
            }
        });

        firestore = FirebaseFirestore.getInstance();
    }
    public void registration_but_click(View view){
        name = ((EditText)findViewById(R.id.name_registration)).getText().toString();
        phone_number = ((EditText)findViewById(R.id.phone_number_registration)).getText().toString();
        email = ((EditText)findViewById(R.id.e_mail_registration)).getText().toString();
        password = ((EditText)findViewById(R.id.password_registration)).getText().toString();
        password_again = ((EditText)findViewById(R.id.password_again_registration)).getText().toString();

        if(((RadioButton)findViewById(R.id.rb_entrepreneur)).isChecked())
            status = true;
        else
            status = false;

        Log.i("New user",String.valueOf(status));
        createNewUser();
    }
    private void createNewUser(){
        if(!validateForm())
            return;

        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser user = mAuth.getCurrentUser();

                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name)
                            .build();

                    user.updateProfile(profileUpdates);

                    updateUI(user);
                }else{
                    Toast.makeText(getBaseContext(),"ERROR",Toast.LENGTH_LONG).show();
                    updateUI(null);
                }
                hideProgressDialog();
            }
        });
    }
    public void setAvatar(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        startActivityForResult(Intent.createChooser(photoPickerIntent,"Select avatar"),1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==1 && data!=null){
            Uri selectedImage = data.getData();
            String filePathColumn[] = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn,null,null,null);

            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();

            uAvatar = BitmapFactory.decodeFile(imagePath);
        }
    }

    private boolean validateForm(){
        if(name.isEmpty()){
            Toast.makeText(this,R.string.name_empty,Toast.LENGTH_LONG).show();
            return false;
        }
        if(phone_number.isEmpty()){
            Toast.makeText(this,R.string.phone_number_empty,Toast.LENGTH_LONG).show();
            return false;
        }
        if(email.isEmpty()){
            Toast.makeText(this,R.string.e_mail_empty,Toast.LENGTH_LONG).show();
            return false;
        }
        if(password.isEmpty()){
            Toast.makeText(this,R.string.password_empty,Toast.LENGTH_LONG).show();;
            return false;
        }
        if(password_again.isEmpty()){
            Toast.makeText(this,R.string.password_again_empty,Toast.LENGTH_LONG).show();;
            return false;
        }

        if(password.compareTo(password_again)!=0){
            Toast.makeText(this,R.string.passwordsNotEqual,Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    private void updateUI(final FirebaseUser user){
        if(user!=null) {
            final LinearLayout linearLayout = findViewById(R.id.registration_layout);

            /*
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference ref = storage.getReference().child("users_avatar/" + user.getUid() + ".png");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            uAvatar.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] data = baos.toByteArray();

            ref.putBytes(data)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getBaseContext(),"Upload avatar error",Toast.LENGTH_LONG).show();
                }
            });
*/
            final Map<String,Object> map = new HashMap<>();
            map.put("status",status);
            map.put("name",name);
            map.put("phone",phone_number);
            map.put("userId",user.getUid());
            map.put("e-mail",email);
           // map.put("imageLink",ref.toString());

                    firestore.collection("users")
                            .add(map)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                    Toast.makeText(getBaseContext(),"Данные успешно добавлены",Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getBaseContext(),"Данные не добавлены",Toast.LENGTH_LONG).show();
                                }
                            });
                    sendEmailVerification();
                }
            else{
            Toast.makeText(this,"Ошибка",Toast.LENGTH_LONG).show();
        }
    }
    public void sendEmailVerification(){
        final FirebaseUser user = mAuth.getCurrentUser();

        LinearLayout verLayout = findViewById(R.id.verification_layout);
        LinearLayout regLayout = findViewById(R.id.registration_layout);

        regLayout.setVisibility(View.GONE);
        verLayout.setVisibility(View.VISIBLE);

        final TextView verText = findViewById(R.id.verification_text);

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        verText.setText("Письмо отправлено на " + user.getEmail() + "\t" + getString(R.string.verification_status_failed));
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        verText.setText("Ошибка отправки!");
                    }
                });
    }
    public void checkStatus(View view){
        FirebaseUser user = mAuth.getCurrentUser();
        user.reload();

        if(user.isEmailVerified()){
            Toast.makeText(this,"Успешно!",Toast.LENGTH_LONG).show();
            new Thread(()->{
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra("AsUser",true);
                intent.putExtra("Name",name);
                intent.putExtra("E-mail",email);

                startActivity(intent);
            }).start();
        }
        else
            Toast.makeText(this,"Не успешно!",Toast.LENGTH_LONG).show();

    }
    public void continueAsUser(View view){
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("AsUser",true);
        intent.putExtra("Name",name);
        intent.putExtra("E-mail",email);

        startActivity(intent);
    }
}
