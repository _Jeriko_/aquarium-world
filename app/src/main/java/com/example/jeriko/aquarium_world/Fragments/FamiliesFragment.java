package com.example.jeriko.aquarium_world.Fragments;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.Beans.Family;
import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.FilesDownloader;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.example.jeriko.aquarium_world.R;

import java.util.ArrayList;
import java.util.List;

public class FamiliesFragment extends Fragment {
    private RecyclerView familyRecyclerView;
    private FamilyAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        familyRecyclerView = view.findViewById(R.id.recyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        familyRecyclerView.setLayoutManager(layoutManager);

        checkFamiliesTable();

        return view;
    }
    public void updateUI(List list){
        adapter = new FamilyAdapter(list);
        familyRecyclerView.setAdapter(adapter);
    }
    private class FamilyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView image;
        private TextView title;
        private LinearLayout layout;
        private Family family;

        public FamilyHolder(LayoutInflater inflater,ViewGroup parent) {
            super(inflater.inflate(R.layout.family,parent,false));

            this.image = itemView.findViewById(R.id.family_image);
            this.title = itemView.findViewById(R.id.family_title);

            this.layout = itemView.findViewById(R.id.family_layout);

            this.layout.setOnClickListener(this);
        }
        public void bind(Family family){
            this.family = family;
            Bitmap image = BitmapFactory.decodeByteArray(family.getImage(),0,family.getImage().length);
            this.title.setText(family.getTitle());
            this.image.setImageBitmap(image);

        }
        @Override
        public void onClick(View v) {
            String f=null;

            Bundle bundle = new Bundle();
            switch (family.getId()){
                case 1: break;
                case 2: break;
                case 3: break;
                case 4:
                    f = "cichlidae";
                    bundle.putString("Family",f);
                    break;
                case 5: break;
                case 6: break;
                case 7: break;
                case 8: break;
                case 9: break;
                case 10: break;
            }
            Fragment fragment = new FishFragment();
            fragment.setArguments(bundle);

            FragmentManager manager = getFragmentManager();

            manager.beginTransaction()
                    .replace(R.id.container_recycler,fragment)
                    .addToBackStack(null)
                    .commit();

            //Toast.makeText(getActivity(),"CLICK",Toast.LENGTH_LONG).show();
           /* SpeciesFragment fragment = new SpeciesFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable("Family",family);
            fragment.setArguments(bundle);
            FragmentManager fm = getFragmentManager();
            Fragment f = fm.findFragmentById(R.id.container_recycler);

            fm.beginTransaction()
                    .remove(f)
                    .add(R.id.container_recycler,fragment)
                    .commit();*/
        }
    }
    private class FamilyAdapter extends RecyclerView.Adapter<FamilyHolder>{
        public List<Family> list;
        public FamilyAdapter(List<Family> list){
            this.list = list;
        }
        @NonNull
        @Override
        public FamilyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new FamilyHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull FamilyHolder familyHolder, int i) {
            familyHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
    private void checkFamiliesTable(){
        final DBHelper dbHelper = new DBHelper(getContext());

        Cursor cursor = dbHelper.getReadableDatabase().query("families",null,null,null,null,null,null);

        if(cursor.getCount()==0){
            FilesDownloader downloader = FilesDownloader.getInstance(getContext());
            downloader.getFiles("families","families/families.json","families","json","families").addOnSuccessFilesDownloadListener(new OnSuccessFilesDownloadListener() {
                @Override
                public void onSuccess(final List<DataType> list) {
                    Runnable save = new Runnable() {
                        @Override
                        public void run() {
                            dbHelper.addData("families",list);
                        }
                    };
                    Thread nt = new Thread(save);
                    nt.start();

                    updateUI(list);
                }
            }).addOnFailureFilesDownloadListener(new OnFailureFilesDownloadListener() {
                @Override
                public void onFailure(String message) {

                }
            });
        }else{
            List<Family> list = new ArrayList<>();

            while(cursor.moveToNext())
               //System.out.println(data.getInt(0) + "\t" + data.getString(1));
                list.add(new Family(cursor.getInt(0),cursor.getString(1),cursor.getBlob(2)));

            updateUI(list);
        }
    }
}
