package com.example.jeriko.aquarium_world;

import com.example.jeriko.aquarium_world.Beans.Product;

import java.util.ArrayList;

public class Basket {
    private ArrayList<Product> list;
    private static Basket basket;

    private Basket(){
        list = new ArrayList<>();
    }
    public static Basket getInstance(){
        if(basket==null)
            basket = new Basket();
        return basket;
    }

    public ArrayList<Product> getList() {
        return list;
    }
}
