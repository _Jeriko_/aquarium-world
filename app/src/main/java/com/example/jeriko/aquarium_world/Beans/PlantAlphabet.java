package com.example.jeriko.aquarium_world.Beans;

import java.util.ArrayList;

public class PlantAlphabet {
    private int id;
    private String letter;
    private byte[] image;
    private int count;

    public PlantAlphabet(int id,String letter,byte[]image,int count){
        this.id = id;
        this.letter = letter;
        this.image = image;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public String getLetter() {
        return letter;
    }

    public byte[] getImage() {
        return image;
    }

    public int getCount() {
        return count;
    }
}
