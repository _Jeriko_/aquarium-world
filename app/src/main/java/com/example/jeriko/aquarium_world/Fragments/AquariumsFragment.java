package com.example.jeriko.aquarium_world.Fragments;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.Aquarium;
import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.FilesDownloader;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.example.jeriko.aquarium_world.R;

import java.util.ArrayList;
import java.util.List;

public class AquariumsFragment extends Fragment {
    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(layoutManager);

        checkAquariumTable();

        return view;
    }
    public void updateUI(List list){
        AquariumAdapter adapter = new AquariumAdapter(list);
        recyclerView.setAdapter(adapter);
    }
    private class AquariumHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView aquariumImage;
        private TextView aquariumType;
        private LinearLayout aquariumLayout;

        public AquariumHolder(LayoutInflater inflater,ViewGroup container) {
            super(inflater.inflate(R.layout.aquarium,container,false));

            aquariumLayout = itemView.findViewById(R.id.aquarium_layout);
            aquariumImage = itemView.findViewById(R.id.aquarium_image);
            aquariumType = itemView.findViewById(R.id.aquarium_title);

            aquariumLayout.setOnClickListener(this);
        }
        public void bind(Aquarium aquarium){
            Bitmap image = BitmapFactory.decodeByteArray(aquarium.getImage(),0,aquarium.getImage().length);
            aquariumImage.setImageBitmap(image);
            aquariumType.setText(aquarium.getTitle());
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(),"CLICK",Toast.LENGTH_LONG).show();
        }
    }
    private class AquariumAdapter extends RecyclerView.Adapter<AquariumHolder>{
        private List<Aquarium> list;
        public AquariumAdapter(List<Aquarium> list){
            this.list = list;
        }
        @NonNull
        @Override
        public AquariumHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new AquariumHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull AquariumHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
    private void checkAquariumTable(){
        final DBHelper dbHelper = new DBHelper(getContext());

        Cursor cursor = dbHelper.getReadableDatabase().query("aquariums",new String[] {"_id"},null,null,null,null,null);

        if(cursor.getCount()==0){
            FilesDownloader downloader = FilesDownloader.getInstance(getContext());
            downloader.getFiles("aquariums","aquariums/aquariums.json","aquariums","json","aquariums").addOnSuccessFilesDownloadListener(new OnSuccessFilesDownloadListener() {
                @Override
                public void onSuccess(final List<DataType> list) {
                    Runnable save = new Runnable() {
                        @Override
                        public void run() {
                            dbHelper.addData("aquariums",list);
                        }
                    };
                    Thread nt = new Thread(save);
                    nt.start();

                    Toast.makeText(getContext(),String.valueOf(list.size()),Toast.LENGTH_LONG).show();
                    updateUI(list);
                }
            }).addOnFailureFilesDownloadListener(new OnFailureFilesDownloadListener() {
                @Override
                public void onFailure(String message) {

                }
            });
        }else{
            List<Aquarium> list = new ArrayList<>();
            Cursor data = dbHelper.getReadableDatabase().query("aquariums",null,null,null,null,null,null);

            while(data.moveToNext())
                list.add(new Aquarium(data.getInt(0),data.getString(1),null,data.getBlob(2)));

            updateUI(list);
        }
    }
}
