package com.example.jeriko.aquarium_world.Beans;

import com.example.jeriko.aquarium_world.Beans.DataType;

public class Plant extends DataType {
    public Plant(int id,String name,String imageName,byte[]image){
        super(id,name,imageName,image);
    }
    public Plant(int id,String name,byte[]image){
        super(id,name,image);
    }
}
