package com.example.jeriko.aquarium_world;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.Beans.FishP;
import com.example.jeriko.aquarium_world.Beans.Product;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SellersSelectionDialogFragment extends DialogFragment {
    private RecyclerView recyclerView;
    private AlertDialog chooser;
    private String name;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        name = getArguments().getString("Name");
        Log.i("Product name",name);

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.sellers_selection,null);


        recyclerView = view.findViewById(R.id.sellers_selection_recycler);
        //((TextView)view.findViewById(R.id.sellers_selection_title)).setText(name);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new SelectedUserAdapter(((List<Product>)getArguments().getSerializable("List"))));

        builder.setView(view)
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i("Select seller","Cancel");
                    }
                });
        chooser = builder.create();

        return chooser;
    }
    private class SelectedUserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout productLayout;
        private ImageView productImage;
        private TextView productName;
        private TextView productPrice;
        private TextView productAmount;
        private TextView sellerName;

        public SelectedUserHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.selected_product,container,false));

            productLayout = itemView.findViewById(R.id.product_selection_layout);
            productImage = itemView.findViewById(R.id.product_selection_image);
            productName = itemView.findViewById(R.id.product_selection_name);
            productAmount = itemView.findViewById(R.id.product_selection_amount);
            productPrice = itemView.findViewById(R.id.product_selection_cost);
            sellerName = itemView.findViewById(R.id.product_selection_seller_name);

            productLayout.setOnClickListener(this);
        }
        private void bind(Product product){
            productName.setText(product.getName());
            productPrice.setText(productPrice.getText() + String.valueOf(product.getPrice()));
            productAmount.setText(productAmount.getText() + String.valueOf(product.getAmount()));

            FirebaseFirestore firestore = FirebaseFirestore.getInstance();
            firestore.collection("users")
                    .whereEqualTo("userId",product.getUserId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            sellerName.setText(task.getResult().iterator().next().getData().get("name").toString());
                        }
                    });
        }

        @Override
        public void onClick(View v) {
            BuyDialogFragment buyDialogFragment = new BuyDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("Product","Акара");
            buyDialogFragment.setArguments(bundle);
            buyDialogFragment.show(getFragmentManager(),"Count");
        }
    }
    private class SelectedUserAdapter extends RecyclerView.Adapter<SelectedUserHolder>{
        private List<Product>list;

        public SelectedUserAdapter(List<Product>list){
            this.list = list;
        }
        @NonNull
        @Override
        public SelectedUserHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new SelectedUserHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull SelectedUserHolder selectedUserHolder, int i) {
            selectedUserHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
