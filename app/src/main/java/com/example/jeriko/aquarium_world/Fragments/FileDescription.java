package com.example.jeriko.aquarium_world.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.Beans.FishP;
import com.example.jeriko.aquarium_world.Beans.Product;
import com.example.jeriko.aquarium_world.BuyDialogFragment;
import com.example.jeriko.aquarium_world.R;
import com.example.jeriko.aquarium_world.SellersSelectionDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FileDescription extends Fragment {
    private String name;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fish_info,container,false);

        Bundle bundle = getArguments();

        char[] description = bundle.getCharArray("Description");
        name = bundle.getString("Name");
        Bitmap image = BitmapFactory.decodeByteArray(bundle.getByteArray("Image"),0,bundle.getByteArray("Image").length);

        ((ImageView)view.findViewById(R.id.fish_info_image)).setImageBitmap(image);
        ((TextView)view.findViewById(R.id.fish_info_description)).setText(new String(description));
        ((TextView)view.findViewById(R.id.fish_info_name)).setText(name);


        ((Button)view.findViewById(R.id.fish_info_buy)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseFirestore firestore = FirebaseFirestore.getInstance();
                List<Product> list = new ArrayList<>();
                firestore.collection("products")
                        .whereEqualTo("name",name)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                int index=0;
                                for(QueryDocumentSnapshot t : task.getResult()){
                                    System.out.println("ADD");
                                    list.add(new FishP(index++,t.getData().get("name").toString(),Float.valueOf(t.getData().get("price").toString()),Integer.valueOf(t.getData().get("amount").toString()),null,t.getData().get("userId").toString()));
                                }
                                listDownloaded(list);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i("Get data","Failure");
                    }
                });
            }
        });
        return view;
    }
    private void listDownloaded(List<Product> list){
        SellersSelectionDialogFragment fragment = new SellersSelectionDialogFragment();
        Bundle b = new Bundle();
        b.putSerializable("List", (Serializable) list);
        b.putString("Name",name);
        fragment.setArguments(b);
        fragment.show(getFragmentManager(),"Seller selection");
    }
}
