package com.example.jeriko.aquarium_world;

import java.util.ArrayList;

public class FileImages {
    private String text;
    private ArrayList<byte[]>images;

    public FileImages(String text){
        this.text = text;
        images = new ArrayList<>();
    }
    public void add(byte[] image){
        images.add(image);
    }
}
