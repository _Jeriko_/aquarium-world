package com.example.jeriko.aquarium_world.Beans;

public class Seller {
    private int id;
    private String name;
    private byte[]image;
    public Seller(int id,String name,byte[]image){
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getImage() {
        return image;
    }
}
