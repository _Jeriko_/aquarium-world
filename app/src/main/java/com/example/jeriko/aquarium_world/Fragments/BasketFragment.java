package com.example.jeriko.aquarium_world.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.Basket;
import com.example.jeriko.aquarium_world.Beans.Product;
import com.example.jeriko.aquarium_world.R;

import java.util.List;

public class BasketFragment extends Fragment {
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        updateUI();

        return view;
    }
    private void updateUI(){
        recyclerView.setAdapter(new ProductAdapter(Basket.getInstance().getList()));
    }
    private class ProductHolder extends RecyclerView.ViewHolder{
        private LinearLayout productLayout;
        private ImageView productImage;
        private TextView productName;
        private TextView productCount;
        private TextView productCost;

        public ProductHolder(LayoutInflater inflater,ViewGroup group){
            super(inflater.inflate(R.layout.product,group));

            productLayout = itemView.findViewById(R.id.products_list_item_layout);
            productImage = itemView.findViewById(R.id.products_list_item_image);
            productName = itemView.findViewById(R.id.products_list_item_name);
            productCount = itemView.findViewById(R.id.products_list_item_amount);
            productCost = itemView.findViewById(R.id.products_list_item_price);

        }
        private void bind(Product product){

        }
    }
    private class ProductAdapter extends RecyclerView.Adapter<ProductHolder>{
        private List<Product> list;
        public ProductAdapter(List<Product>list){
            this.list = list;
        }
        @NonNull
        @Override
        public ProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new ProductHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull ProductHolder productHolder, int i) {
            productHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

}
