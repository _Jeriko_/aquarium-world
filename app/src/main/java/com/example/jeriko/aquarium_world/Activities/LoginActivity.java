package com.example.jeriko.aquarium_world.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private String email;
    private String password;
    private static final String TAG = "EmailPassword";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    @Override
    protected void onResume(){
        super.onResume();
        ((Toolbar)findViewById(R.id.toolbar)).setTitle(R.string.entrance);
    }

    public void onlyView(View view){
        Intent mainPage = new Intent(this,MainActivity.class);
        mainPage.putExtra("AsUser",false);
        startActivity(mainPage);
    }
    public void registration(View view){
        Intent regPage = new Intent(this,RegistrationActivity.class);
        startActivity(regPage);
    }
    public void login_but_click(View view){
        email = ((EditText)findViewById(R.id.email_login)).getText().toString();
        password = ((EditText)findViewById(R.id.password_login)).getText().toString();

        signIn(email,password);
    }
    private void signIn(String email,String password){
        if(!validateForm())
            return;

        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    //Toast.makeText(getBaseContext(),"SUCCESSFULLY",Toast.LENGTH_LONG).show();
                    Log.d(TAG,"singInWithEmailPassword:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);

                }else{
                    Toast.makeText(getBaseContext(),"ERROR",Toast.LENGTH_LONG).show();
                    Log.w(TAG,"signInWithEmailPassword:failed",task.getException());
                    updateUI(null);
                }
                hideProgressDialog();
            }
        });
    }
    private boolean validateForm(){
        if(email.isEmpty()){
            Toast.makeText(this,R.string.e_mail_empty,Toast.LENGTH_LONG).show();
            return false;
        }
        if(password.isEmpty()){
            Toast.makeText(this,R.string.password_empty,Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    private void updateUI(FirebaseUser currentUser){
        LinearLayout layout = findViewById(R.id.successfully_login_layout);
        LinearLayout oldLayout = findViewById(R.id.login_layout);
        TextView greeting = findViewById(R.id.greeting);
        if(currentUser!=null){
            //Toast.makeText(this,"Вход произведен!",Toast.LENGTH_LONG).show();
            greeting.setText("Продолжить как " +currentUser.getEmail());

            oldLayout.setVisibility(View.GONE);
            layout.setVisibility(View.VISIBLE);

        } else {
            layout.setVisibility(View.GONE);
            oldLayout.setVisibility(View.VISIBLE);
        }
    }
    public void continueAsUser(View view){
        FirebaseUser user = mAuth.getCurrentUser();

       String email = user.getEmail();
       String name = user.getDisplayName();

        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("AsUser",true);
        intent.putExtra("E-mail",email);
        intent.putExtra("Name",name);
        startActivity(intent);
    }
    public void logout(View view){
        mAuth.signOut();
        updateUI(null);
    }
}