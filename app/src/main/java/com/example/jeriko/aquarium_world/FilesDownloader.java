package com.example.jeriko.aquarium_world;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.jeriko.aquarium_world.Beans.Accessory;
import com.example.jeriko.aquarium_world.Beans.Aquarium;
import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.Beans.Family;
import com.example.jeriko.aquarium_world.Beans.Fish;
import com.example.jeriko.aquarium_world.Beans.Type;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFileDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FilesDownloader {
    private static FilesDownloader downloader;
    private static FirebaseStorage storage;
    private StorageReference reference;
    private ArrayList<DataType> data;
    private static Context context;
    private File jsonFile;
    private static ProgressDialog progressDialog;
    //private EventListener listener;
    private String folder;
    private String dataType;
    private OnFailureFilesDownloadListener failureListener;
    private OnSuccessFilesDownloadListener successListener;  
    private OnSuccessFileDownloadListener fileSuccessListener;

    public static FilesDownloader getInstance(Context aContext){
        if(downloader == null) {
            downloader = new FilesDownloader();
            storage = FirebaseStorage.getInstance();
            context = aContext;
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(true);
            progressDialog.setTitle(R.string.loading);
        }
        progressDialog.show();
        return downloader;
    }
    public FilesDownloader getFiles(String dataType,String path, String prefix, String suffix,String folder){
        this.folder = folder;
        this.dataType = dataType;

        this.data = new ArrayList<>();

        reference = storage.getReference().child(path);
        try {
            jsonFile = File.createTempFile(prefix, suffix);

            reference.getFile(jsonFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Log.i("Downloading of file!","Success");
                    readJSON(jsonFile);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i("Downloading of file!","Failure");
                    progressDialog.dismiss();
                }
            });
            return downloader;
        }catch (IOException ex){
            Log.e("File's creating","Error");
            return null;
        }
    }
    public FilesDownloader downloadImages(final ArrayList<String> path, final String folder){
        final long ONE_MEGABYTE = 1024 * 1024;
        final int index[] = {0};

        for(final String s : path){
            reference = storage.getReference().child(folder + '/' + s);

            reference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Log.i("File download " + folder, "#" + index[0] + " successfully!");
                    setImage(bytes,s,path.size(),index[0]++);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i("File download " + folder, "#" + index[0] + "failed!");
                    failureListener.onFailure("Error");
                }
            });
        }
        return downloader;
    }
    private void readJSON(File file){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
        }

        String json=null;
        try {
            byte[]arr = new byte[fis.available()];
            fis.read(arr);

            json = new String(arr);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }

        if(json==null){
            Log.e("JSON file","File is null!");
            return;
        }

        try {
            JSONArray array = new JSONArray(json);

            data = new ArrayList<>(array.length());
            ArrayList<String> imageName = null;

            switch (dataType){
                case "types": imageName = getTypeImages(array);break;
                case "families": imageName = getFamiliesImages(array);break;
                case "aquariums": imageName = getAquariumsImages(array);break;
                case "fish": imageName = getFishImages(array);
                break;
                case "accessories": imageName = getAccessoriesImages(array);break;
                default:
                    Log.e("Selection error","No this option");break;
            }
            // Скачиваем картинки
            downloadImages(imageName,folder);
        }
        catch (JSONException ex){
            ex.printStackTrace();
        }
    }
    private ArrayList<String> getTypeImages(JSONArray array) throws JSONException{
        ArrayList<String> imagesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            data.add(new Type(object.getInt("Id"),object.getString("Title"),object.getString("Image"),null));
            imagesList.add(object.getString("Image"));
        }
        return imagesList;
    }
    private ArrayList<String> getFamiliesImages(JSONArray array) throws JSONException{
        ArrayList<String> imagesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            data.add(new Family(object.getInt("Id"),object.getString("Title"),object.getString("Image"),null));
            imagesList.add(object.getString("Image"));
        }
        return imagesList;
    }
    private ArrayList<String> getAquariumsImages(JSONArray array) throws JSONException{
        ArrayList<String> imagesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            data.add(new Aquarium(object.getInt("Id"),object.getString("Title"),object.getString("Image"),null));
            imagesList.add(object.getString("Image"));
        }
        return imagesList;
    }
    private ArrayList<String> getAccessoriesImages(JSONArray array) throws JSONException{
        ArrayList<String> imagesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            data.add(new Accessory(object.getInt("Id"),object.getString("Title"),object.getString("Image"),null));
            imagesList.add(object.getString("Image"));
        }
        return imagesList;
    }
    private ArrayList<String> getFishImages(JSONArray array) throws JSONException {
        ArrayList<String> imagesList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);

            data.add(new Fish(object.getInt("Id"),object.getString("Title"),folder,null,object.getString("Image")));
            imagesList.add(object.getString("Image"));
        }
        return imagesList;
    }
    private void setImage(byte[] image,String name, int size,int index){
        int i=0;
        for(DataType dt : data) {
            Log.i("SET",dt.getImageName() + "\t" + name);
            if (dt.getImageName().contains(name)) {

                data.get(i).setImage(image);
                break;
            }
            i++;
        }
        if(index==size-1) {
            progressDialog.dismiss();
            successListener.onSuccess(data);
        }
    }

    public FilesDownloader addOnSuccessFilesDownloadListener(OnSuccessFilesDownloadListener downloadListener){
        this.successListener = downloadListener;
        return downloader;
    }
    public FilesDownloader addOnFailureFilesDownloadListener(OnFailureFilesDownloadListener downloadListener){
        this.failureListener = downloadListener;
        return downloader;
    }
    public FilesDownloader addOnSuccessFileDownloadListener(OnSuccessFileDownloadListener downloadListener){
        this.fileSuccessListener = downloadListener;
        return downloader;
    }
    public FilesDownloader getDescriptionFile(String path,String imageName){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        String prefix = imageName.substring(0,imageName.indexOf('.'));
        StorageReference reference = storage.getReference().child(path + "/" + prefix + ".txt");
        try {
            File descriptionFile = File.createTempFile(prefix,"txt");

            reference.getFile(descriptionFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    fileSuccessListener.OnSuccess(descriptionFile);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Log.e("File download","Failed");
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return downloader;
    }
}