package com.example.jeriko.aquarium_world.Fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.Beans.PlantAlphabet;
import com.example.jeriko.aquarium_world.R;

import java.util.ArrayList;

public class PlantsFragment extends Fragment {
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),4);
        recyclerView.setLayoutManager(layoutManager);

        updateUI();

        return view;
    }
    public void updateUI(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query("plants",null,null,null,null,null,null);

        if(cursor.getCount()==0){
            Log.e("Count","Count of rows is null");
            return;
        }
        ArrayList<PlantAlphabet> list = new ArrayList<>();

        while (cursor.moveToNext())
            list.add(new PlantAlphabet(cursor.getInt(0),cursor.getString(1),cursor.getBlob(3),cursor.getInt(2)));

        recyclerView.setAdapter(new PlantsAdapter(list));
    }
    private class PlantsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView plantCardView;
        private TextView title;
        private TextView letter;
        private TextView count;

        public PlantsHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.plant,container,false));

            plantCardView = itemView.findViewById(R.id.plant_alphabet_card);
            title = itemView.findViewById(R.id.plant_alphabet_image);
            letter = itemView.findViewById(R.id.plant_alphabet_letter);
            count = itemView.findViewById(R.id.plant_alphabet_count);

            plantCardView.setOnClickListener(this);
        }
        private void bind(PlantAlphabet plant){
            letter.setText(plant.getLetter());
            count.setText(String.valueOf(plant.getCount()));
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(),"CLICK",Toast.LENGTH_LONG).show();
        }
    }
    private class PlantsAdapter extends RecyclerView.Adapter<PlantsHolder>{
        private ArrayList<PlantAlphabet> list;
        public PlantsAdapter(ArrayList<PlantAlphabet>list){
            this.list = list;
        }
        @NonNull
        @Override
        public PlantsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new PlantsHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull PlantsHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }

}
