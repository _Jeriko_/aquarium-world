package com.example.jeriko.aquarium_world.Fragments;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.Beans.Type;
import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.FilesDownloader;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.example.jeriko.aquarium_world.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TypesFragment extends Fragment {
    private RecyclerView typesRecyclerView;
    private TypeAdapter adapter;
    private File jsonFile;
    private ArrayList<Type> list;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        typesRecyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        typesRecyclerView.setLayoutManager(layoutManager);

        checkTypesTable();
        return view;
    }
    private void updateUI(List list){
        adapter = new TypeAdapter(list);

        typesRecyclerView.setAdapter(adapter);
    }
    private class TypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView typeName;
        private ImageView typeImage;
        private Type type;
        private LinearLayout layout;

        public TypeHolder(LayoutInflater inflater,ViewGroup parent) {
            super(inflater.inflate(R.layout.type,parent,false));

            typeName = itemView.findViewById(R.id.type_name);
            typeImage = itemView.findViewById(R.id.type_image);
            layout = itemView.findViewById(R.id.type_layout);
        }
        public void bind(Type type){
            this.type = type;
            typeName.setText(type.getTitle());
            Bitmap bitmap = BitmapFactory.decodeByteArray(type.getImage(),0,type.getImage().length);
            typeImage.setImageBitmap(bitmap);

            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (type.getId()){
                case 1:
                    changeMainFragment(new FamiliesFragment());
                    break;
                case 2:
                    changeMainFragment(new PlantsFragment());
                    break;
                case 3:
                    changeMainFragment(new AquariumsFragment());
                    break;
                case 4:
                    changeMainFragment(new AccessoriesFragment());
                    break;
                case 5:
                    changeMainFragment(new SundryFragment());
                    break;
            }
        }
    }
    private class TypeAdapter extends RecyclerView.Adapter<TypeHolder>{
        private List<Type> list;
        public TypeAdapter(List<Type> list){
            this.list = list;
        }
        @NonNull
        @Override
        public TypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new TypeHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull TypeHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
    private void changeMainFragment(Fragment newFragment){
        FragmentManager fm = getFragmentManager();

        fm.beginTransaction()
                .replace(R.id.container_recycler,newFragment)
                .addToBackStack(null)
                .commit();
    }
    // Проберка таблицы
    private void checkTypesTable() {
        final DBHelper dbHelper = new DBHelper(getContext());
        Cursor cursor = dbHelper.getReadableDatabase().query("types",null, null,null,null,null,null);

        final String rows[] = {"_id","title","image"};

        // Если база только создана
        if(cursor.getCount()==0){
            Log.i("Types' table","0 records in the table!");

            FilesDownloader downloader = FilesDownloader.getInstance(getContext());
            downloader.getFiles("types","types/types.json","types","json","types").addOnSuccessFilesDownloadListener(new OnSuccessFilesDownloadListener() {
                @Override
                public void onSuccess(final List<DataType> list) {
                    Log.i("Data downloading","Data download was successful");

                    Runnable save = new Runnable() {
                        @Override
                        public void run() {
                            dbHelper.addData("types",list);
                        }
                    };
                    Thread nt = new Thread(save);
                    nt.start();

                    updateUI(list);
                }
            }).addOnFailureFilesDownloadListener(new OnFailureFilesDownloadListener() {
                @Override
                public void onFailure(String message) {
                    Toast.makeText(getContext(),message,Toast.LENGTH_LONG).show();
                }
            });
        }else{  // Если таблица не пуста
            list = new ArrayList<>();


            while(cursor.moveToNext()) {
                list.add(new Type(cursor.getInt(0), cursor.getString(1), cursor.getBlob(2)));
            }

            updateUI(list);
        }
    }
}