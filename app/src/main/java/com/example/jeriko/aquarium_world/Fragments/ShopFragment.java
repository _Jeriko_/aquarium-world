package com.example.jeriko.aquarium_world.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.AccessoryP;
import com.example.jeriko.aquarium_world.Beans.AquariumP;
import com.example.jeriko.aquarium_world.Beans.FishP;
import com.example.jeriko.aquarium_world.Beans.PlantP;
import com.example.jeriko.aquarium_world.Beans.Product;
import com.example.jeriko.aquarium_world.Beans.SundryP;
import com.example.jeriko.aquarium_world.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class ShopFragment extends Fragment {
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        getData();
        return view;
    }
    private void getData(){
        List<Product> list = new ArrayList<>();

        FirebaseFirestore firestore = FirebaseFirestore.getInstance();

        final int[] index = {0};
        firestore.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        for(QueryDocumentSnapshot qds : task.getResult()){
                            switch (qds.getData().get("type").toString()){
                                case "Рыбки":
                                    list.add(new FishP(index[0]++,qds.getData().get("name").toString(),Float.valueOf(qds.getData().get("price").toString()),Integer.valueOf(qds.getData().get("amount").toString()),null,qds.getData().get("userId").toString()));
                                    break;
                                case "Растения":
                                    list.add(new PlantP(index[0]++,qds.getData().get("name").toString(),Float.valueOf(qds.getData().get("price").toString()),Integer.valueOf(qds.getData().get("amount").toString()),null,qds.getData().get("userId").toString()));
                                    break;
                                case "Аквариумы":
                                    list.add(new AquariumP(index[0]++,qds.getData().get("name").toString(),Float.valueOf(qds.getData().get("price").toString()),Integer.valueOf(qds.getData().get("amount").toString()),null,qds.getData().get("userId").toString()));
                                    break;
                                case "Аксессуары":
                                    list.add(new AccessoryP(index[0]++,qds.getData().get("name").toString(),Float.valueOf(qds.getData().get("price").toString()),Integer.valueOf(qds.getData().get("amount").toString()),null,qds.getData().get("userId").toString()));
                                    break;
                                case "Разное":
                                    list.add(new SundryP(index[0]++,qds.getData().get("name").toString(),Float.valueOf(qds.getData().get("price").toString()),Integer.valueOf(qds.getData().get("amount").toString()),null,qds.getData().get("userId").toString()));
                                    break;
                            }
                        }
                        updateUI(list);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
    private void updateUI(List<Product>list){
        recyclerView.setAdapter(new ProductAdapter(list));
    }
    private class ProductHolder extends RecyclerView.ViewHolder{
        private LinearLayout productLayout;
        private TextView productPrice;
        private TextView productAmount;
        private TextView productName;
        private TextView productSeller;
        private ImageView productImage;

        public ProductHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.product,container,false));

            productLayout = itemView.findViewById(R.id.products_list_item_layout);
            productName = itemView.findViewById(R.id.products_list_item_name);
            productAmount = itemView.findViewById(R.id.products_list_item_amount);
            productPrice = itemView.findViewById(R.id.products_list_item_price);
            productSeller = itemView.findViewById(R.id.products_list_item_seller);
            productImage = itemView.findViewById(R.id.products_list_item_image);
        }
        private void bind(Product product){
            productName.setText(product.getName());
            productPrice.setText(productPrice.getText() + String.valueOf(product.getPrice()));
            productAmount.setText(productAmount.getText() + String.valueOf(product.getAmount()));

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference ref = storage.getReference().child("products_photo/" + productName.getText() + product.getUserId() + ".png");

            ref.getBytes(1024*1024)
                    .addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                            productImage.setImageBitmap(bitmap);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(),"Failure download of product avatar",Toast.LENGTH_LONG).show();
                }
            });
            FirebaseFirestore firestore = FirebaseFirestore.getInstance();
            firestore.collection("users")
                    .whereEqualTo("userId",product.getUserId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            productSeller.setText(task.getResult().iterator().next().getData().get("name").toString());
                        }
                    });
        }
    }
    private class ProductAdapter extends RecyclerView.Adapter<ProductHolder>{
        private List<Product> list;
        public ProductAdapter(List<Product>list){
            this.list = list;
        }
        @NonNull
        @Override
        public ProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new ProductHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull ProductHolder productHolder, int i) {
            productHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}