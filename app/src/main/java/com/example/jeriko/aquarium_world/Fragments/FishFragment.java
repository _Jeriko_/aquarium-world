package com.example.jeriko.aquarium_world.Fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.Beans.Fish;
import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.FilesDownloader;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.example.jeriko.aquarium_world.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FishFragment extends Fragment {
    private RecyclerView recyclerView;
    private String family;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        family = getArguments().getString("Family");

        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        checkFishTable();

        return view;
    }
    private void updateUI(List list){
        recyclerView.setAdapter(new FishAdapter(list));
    }
    private void checkFishTable(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query("fish",null,null,null,null,null,null);

        if(cursor.getCount()==0){
            FilesDownloader downloader = FilesDownloader.getInstance(getContext());
            downloader.getFiles("fish","families/" + family + "/" + family + ".json",family,"json","families/"+family)
                    .addOnSuccessFilesDownloadListener((list)->{
                        new Thread(()->{
                            dbHelper.addData("fish",list);
                        }).start();

                        updateUI(list);
                    })
                    .addOnFailureFilesDownloadListener((message -> Log.e("Download of files",message)));

        }else{
            List<Fish> list = new ArrayList<>();
            while(cursor.moveToNext()){
                list.add(new Fish(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getBlob(3),cursor.getString(4)));
            }
            updateUI(list);
        }
    }
    private class FishHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout fishLayout;
        private ImageView fishImage;
        private TextView fishName;
        private Fish fish;
        private String family;
        private String imageName;

        public FishHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.fish,container,false));

            fishLayout = itemView.findViewById(R.id.fish_layout);
            fishImage = itemView.findViewById(R.id.fish_image);
            fishName = itemView.findViewById(R.id.fish_name);
        }
        private void bind(Fish fish){
            this.fish = fish;
            this.family = fish.getFamily();
            this.imageName = fish.getImageName();

            fishImage.setImageBitmap(BitmapFactory.decodeByteArray(fish.getImage(),0,fish.getImage().length));
            fishName.setText(fish.getName());

            fishLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            FilesDownloader downloader = FilesDownloader.getInstance(getContext());

            downloader.getDescriptionFile(family,imageName)
                    .addOnSuccessFileDownloadListener((file -> openDescription(file)));
        }
        private void openDescription(File file){
            try {
                FileInputStream fis = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fis,"Windows-1251");

                char[]description = new char[fis.available()];
                isr.read(description);

                fis.close();

                FragmentManager fm = getFragmentManager();

                Bundle bundle = new Bundle();
                bundle.putCharArray("Description",description);
                bundle.putByteArray("Image",fish.getImage());
                bundle.putString("Name",fish.getName());

                FileDescription fileDescription = new FileDescription();
                fileDescription.setArguments(bundle);

                fm.beginTransaction()
                        .replace(R.id.container_recycler,fileDescription)
                        .addToBackStack(null)
                        .commit();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class FishAdapter extends RecyclerView.Adapter<FishHolder>{
        private List<Fish> list;
        public FishAdapter(List<Fish> list){
            this.list = list;
        }
        @NonNull
        @Override
        public FishHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new FishHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull FishHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}