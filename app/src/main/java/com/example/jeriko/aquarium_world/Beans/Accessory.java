package com.example.jeriko.aquarium_world.Beans;

public class Accessory extends DataType {
    public Accessory(int id,String title,String imageName,byte[] image){
        super(id,title,imageName,image);
    }
    public Accessory(int id,String title,byte[]image){
        super(id,title,image);
    }
}
