package com.example.jeriko.aquarium_world.Fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeriko.aquarium_world.Beans.Accessory;
import com.example.jeriko.aquarium_world.Beans.DataType;
import com.example.jeriko.aquarium_world.DBHelper;
import com.example.jeriko.aquarium_world.FilesDownloader;
import com.example.jeriko.aquarium_world.Listeners.OnFailureFilesDownloadListener;
import com.example.jeriko.aquarium_world.Listeners.OnSuccessFilesDownloadListener;
import com.example.jeriko.aquarium_world.R;

import java.util.ArrayList;
import java.util.List;

public class AccessoriesFragment extends Fragment {
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        recyclerView = view.findViewById(R.id.recyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);

        recyclerView.setLayoutManager(layoutManager);

        checkAccessoriesTable();

        return  view;
    }
    private void updateUI(List list){
        recyclerView.setAdapter(new AccessoryAdapter(list));
    }
    private void checkAccessoriesTable(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query("accessories",null,null,null,null,null,null);
        if(cursor.getCount()==0){
            FilesDownloader downloader = FilesDownloader.getInstance(getContext());

            downloader.getFiles("accessories","accessories/accessories.json","accessories","json","accessories").addOnSuccessFilesDownloadListener(new OnSuccessFilesDownloadListener() {
                @Override
                public void onSuccess(List<DataType> list) {
                    new Thread(()-> dbHelper.addData("accessories", list)).start();

                    updateUI(list);
                }
            }).addOnFailureFilesDownloadListener(new OnFailureFilesDownloadListener() {
                @Override
                public void onFailure(String message) {
                    Log.e("Download files",message);
                }
            });
        }else{
            List<Accessory> list = new ArrayList<>();
            while(cursor.moveToNext()){
                list.add(new Accessory(cursor.getInt(0),cursor.getString(1),cursor.getBlob(2)));
            }
            updateUI(list);
        }
    }
    private class AccessoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout accessoryLayout;
        private ImageView accessoryImage;
        private TextView accessoryTitle;

        public AccessoryHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(R.layout.accessory,container,false));

            accessoryLayout = itemView.findViewById(R.id.accessory_layout);
            accessoryImage = itemView.findViewById(R.id.accessory_image);
            accessoryTitle = itemView.findViewById(R.id.accessory_title);

            accessoryLayout.setOnClickListener(this);
        }
        private void bind(Accessory accessory){
            Bitmap bitmap = BitmapFactory.decodeByteArray(accessory.getImage(),0,accessory.getImage().length);

            accessoryImage.setImageBitmap(bitmap);
            accessoryTitle.setText(accessory.getTitle());
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(),"CLICK",Toast.LENGTH_LONG).show();
        }
    }
    private class AccessoryAdapter extends RecyclerView.Adapter<AccessoryHolder>{
        private List<Accessory> list;
        public AccessoryAdapter(List<Accessory> list){
            this.list = list;
        }
        @NonNull
        @Override
        public AccessoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            return new AccessoryHolder(inflater,parent);
        }

        @Override
        public void onBindViewHolder(@NonNull AccessoryHolder holder, int position) {
            holder.bind(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
