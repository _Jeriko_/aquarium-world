package com.example.jeriko.aquarium_world.Beans;

import java.io.Serializable;

public abstract class Product implements Serializable {
    private int id;
    private String name;
    private float price;
    private byte[] image;
    private String userId;
    private int amount;

    public Product(int id,String name,float price,int amount,byte[]image,String userId){
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.image = image;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public byte[] getImage() {
        return image;
    }

    public String getUserId() {
        return userId;
    }

    public int getAmount() {
        return amount;
    }
}
