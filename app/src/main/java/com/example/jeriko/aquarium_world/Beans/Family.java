package com.example.jeriko.aquarium_world.Beans;

import com.example.jeriko.aquarium_world.Beans.DataType;

import java.io.Serializable;

public class Family extends DataType {
    public Family(int id,String name,String imageName,byte[] image){
        super(id,name,imageName,image);
    }
    public Family(int id,String name,byte[]image){
        super(id,name,image);
    }
}
