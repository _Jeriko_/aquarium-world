package com.example.jeriko.aquarium_world.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jeriko.aquarium_world.Beans.Species;
import com.example.jeriko.aquarium_world.R;

import java.util.ArrayList;

public class SpeciesFragment extends Fragment {
    private RecyclerView speciesRecyclerView;
    private SpeciesAdapter adapter;
    private ArrayList<Species> list;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view,container,false);

        Bundle arg = getArguments();
        //list = ((Family)arg.get("Family")).getList();

        speciesRecyclerView = view.findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        speciesRecyclerView.setLayoutManager(layoutManager);

        updateUI();
        return view;
    }
    public void updateUI(){
        adapter = new SpeciesAdapter();
        speciesRecyclerView.setAdapter(adapter);
    }

    private class SpeciesHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView title;
        private TextView shortDescription;
        private Button buy_but;
        private Button more_but;

        public SpeciesHolder(LayoutInflater inflater,ViewGroup parent){
            super(inflater.inflate(R.layout.species,parent,false));

            image = itemView.findViewById(R.id.species_image_item);
            title = itemView.findViewById(R.id.species_title_item);
       /*     shortDescription = itemView.findViewById(R.id.species_description_item);

            buy_but = itemView.findViewById(R.id.species_but_buy_item);
            more_but = itemView.findViewById(R.id.species_but_more_item);*/

        }
        public void bind(Species species){
                title.setText(species.getName());

        }
    }
    private class SpeciesAdapter extends RecyclerView.Adapter<SpeciesHolder>{
        public SpeciesAdapter(){

        }
        @NonNull
        @Override
        public SpeciesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new SpeciesHolder(inflater,viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull SpeciesHolder speciesHolder, int i) {
            speciesHolder.bind(list.get(i));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}
