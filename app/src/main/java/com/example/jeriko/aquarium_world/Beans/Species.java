package com.example.jeriko.aquarium_world.Beans;

import android.widget.ImageView;

import com.example.jeriko.aquarium_world.Beans.Family;

public class Species {
    private String name;
    private ImageView image;
    private String text;
    private Family family;

    public Species(String name,ImageView image,String text){
        this.name = name;
        this.image = image;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public ImageView getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}
