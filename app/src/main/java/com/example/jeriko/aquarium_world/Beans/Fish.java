package com.example.jeriko.aquarium_world.Beans;

import com.example.jeriko.aquarium_world.Beans.DataType;

public class Fish extends DataType{
   /* public Fish(int id,String name,String imageName,String descriptionFile){
        super(id,name,imageName,descriptionFile);
    }*/
    public Fish(int id,String name,String family,byte[]image,String imageName){
        super(id,name,family,image,imageName);
    }
}