package com.example.jeriko.aquarium_world.Beans;

import java.io.Serializable;
import java.util.LinkedList;

public abstract class DataType implements Serializable{
    private int id;
    private String title;
    private byte[] image;
    private String name;
    private String family;
    private String imageName;
    private String description;
    private String descriptionFile;
    private LinkedList<byte[]> images;

    public DataType(int id,String title,byte[]image){
        this.id = id;
        this.title = title;
        this.image = image;
    }
    public DataType(int id,String title,String imageName,byte[]image){
        this.id = id;
        this.title = title;
        this.imageName = imageName;
        this.image = image;
    }
    public DataType(int id,String name,String family,String imageName,byte[]image){
        this.id = id;
        this.name = name;
        this.family = family;
        this.imageName = imageName;
        this.image = image;
    }
    public DataType(int id,String name,String family,byte[]image,String imageName){
        this.id = id;
        this.name = name;
        this.imageName = imageName;
        this.image = image;
        this.family = family;
    }
    public DataType(int id, String name,String imageName,String descriptionFile,String description) {
        this.id = id;
        this.name = name;
        this.imageName = imageName;
        this.description = description;
        this.descriptionFile = descriptionFile;
    }
    public DataType(int id, String title,String imageName, byte[] image, String description, LinkedList<byte[]> images) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.imageName = imageName;
        this.description = description;
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public byte[] getImage() {
        return image;
    }

    public String getImageName() {
        return imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinkedList<byte[]> getImages() {
        return images;
    }

    public void setImages(LinkedList<byte[]> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionFile() {
        return descriptionFile;
    }

    public void setDescriptionFile(String descriptionFile) {
        this.descriptionFile = descriptionFile;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }
}
