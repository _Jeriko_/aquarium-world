package com.example.jeriko.aquarium_world;

import com.example.jeriko.aquarium_world.Beans.PlantAlphabet;

import java.util.ArrayList;

public class PlantsAlphabetData {
    private static PlantsAlphabetData alphabet;
    private ArrayList<PlantAlphabet> plantAlphabets;

    private PlantsAlphabetData(){
        plantAlphabets = new ArrayList<>();

        plantAlphabets.add(new PlantAlphabet(1,"А",null,0));
        plantAlphabets.add(new PlantAlphabet(2,"Б",null,0));
        plantAlphabets.add(new PlantAlphabet(3,"В",null,0));
        plantAlphabets.add(new PlantAlphabet(4,"Г",null,0));
        plantAlphabets.add(new PlantAlphabet(5,"Д",null,0));
        plantAlphabets.add(new PlantAlphabet(6,"Е",null,0));
        plantAlphabets.add(new PlantAlphabet(7,"Ё",null,0));
        plantAlphabets.add(new PlantAlphabet(8,"Ж",null,0));
        plantAlphabets.add(new PlantAlphabet(9,"З",null,0));
        plantAlphabets.add(new PlantAlphabet(10,"И",null,0));
        plantAlphabets.add(new PlantAlphabet(11,"К",null,0));
        plantAlphabets.add(new PlantAlphabet(12,"Л",null,0));
        plantAlphabets.add(new PlantAlphabet(13,"М",null,0));
        plantAlphabets.add(new PlantAlphabet(14,"Н",null,0));
        plantAlphabets.add(new PlantAlphabet(15,"О",null,0));
        plantAlphabets.add(new PlantAlphabet(16,"П",null,0));
        plantAlphabets.add(new PlantAlphabet(17,"Р",null,0));
        plantAlphabets.add(new PlantAlphabet(18,"С",null,0));
        plantAlphabets.add(new PlantAlphabet(19,"Т",null,0));
        plantAlphabets.add(new PlantAlphabet(20,"У",null,0));
        plantAlphabets.add(new PlantAlphabet(21,"Ф",null,0));
        plantAlphabets.add(new PlantAlphabet(22,"Х",null,0));
        plantAlphabets.add(new PlantAlphabet(23,"Ц",null,0));
        plantAlphabets.add(new PlantAlphabet(24,"Ч",null,0));
        plantAlphabets.add(new PlantAlphabet(25,"Ш",null,0));
        plantAlphabets.add(new PlantAlphabet(26,"Щ",null,0));
        plantAlphabets.add(new PlantAlphabet(27,"Э",null,0));
        plantAlphabets.add(new PlantAlphabet(28,"Ю",null,0));
        plantAlphabets.add(new PlantAlphabet(29,"Я",null,0));
    }
    public static PlantsAlphabetData get(){
        if(alphabet==null)
            alphabet = new PlantsAlphabetData();
        return alphabet;
    }
    public ArrayList<PlantAlphabet> getList(){
        return plantAlphabets;
    }

}
